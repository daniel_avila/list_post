import apiInstance from "../lib/apiInstance";

const getAll = async (filter) => {
  const { data } = await apiInstance.get("/posts");

  if (filter) return data.filter((post) => post.body.includes(filter));
  return data;
};

const getById = async (id) => {
  const { data } = await apiInstance.get("/posts", { id });
  return data;
};

export { getAll, getById };
