import { Row, Container, Col, Card, Button } from "react-bootstrap";

const PostDetail = ({ post, handleBack }) => {
  const { title, body } = post;
  return (
    <Container>
      <Card>
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{body}</Card.Text>
          <Button
            onClick={() => {
              handleBack();
            }}
          >
            Atras
          </Button>
        </Card.Body>
      </Card>
    </Container>
  );
};

export default PostDetail;
