import { Row, Container, Col, Card, Button } from "react-bootstrap";

const PostList = ({ posts, openDetail }) => {
  return (
    <Container className="mt-5">
      <Row xs={1} md={2} className="g-4">
        {posts.map(({ id, title, body }) => (
          <Col key={`post-${id}`} md="3">
            <Card className="post-card">
              <Card.Body className="post-card-body">
                <Card.Title>{title}</Card.Title>
                <Card.Text className="post-card-text">{body}</Card.Text>
                <Button onClick={() => openDetail(id)}>Detalle</Button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default PostList;
