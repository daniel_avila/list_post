import { useState, useEffect } from "react";
import "./App.css";
import { getAll } from "./services/posts";
import PostList from "./components/PostsList";
import PostDetail from "./components/PostDetail";
import { Button, InputGroup, FormControl, Row, Col } from "react-bootstrap";

function App() {
  const [posts, setPosts] = useState([]);
  const [currentFilter, setCurrentFilter] = useState("");
  const [selectedPost, setSelectedPost] = useState();

  async function fetchPosts(filter) {
    const response = await getAll(filter);
    setPosts(response);
  }

  function handleFilter() {
    fetchPosts(currentFilter);
  }

  function handleBack() {
    setSelectedPost();
  }

  function handleOpenDetail(postId) {
    const post = posts.find((post) => post.id === postId);
    setSelectedPost(post);
  }

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div className="App mt-5">
      {selectedPost ? (
        <PostDetail post={selectedPost} handleBack={handleBack} />
      ) : (
        <>
          <Row>
            <Col md={{ span: 6, offset: 3 }}>
              <InputGroup className="mb-3">
                <FormControl
                  placeholder="Filtrar"
                  aria-label="Filtrar"
                  aria-describedby="basic-addon2"
                  value={currentFilter}
                  onChange={(e) => {
                    setCurrentFilter(e.target.value);
                  }}
                />
                <Button
                  variant="outline-secondary"
                  id="button-addon2"
                  onClick={handleFilter}
                >
                  Filtrar
                </Button>
              </InputGroup>
            </Col>
          </Row>
          <PostList posts={posts} openDetail={handleOpenDetail} />
        </>
      )}
    </div>
  );
}

export default App;
